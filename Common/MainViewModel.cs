﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Common.Annotations;
using MvvmCross.Core.ViewModels;

namespace Common
{
    public interface IAlertDisplayer
    {
        void DisplayAlert(string title, string message);
    }

    public class LoginViewModel : MvxViewModel
    {
        public IMvxCommand NavigateToMainCommand => new MvxCommand(() =>
        {
            ShowViewModel<MainViewModel>();
        });
    }

    public class MainViewModel : MvxViewModel
    {
        private readonly IAlertDisplayer _alertDisplayer;
        private string _email;
        private string _password;
          
        public MainViewModel(IAlertDisplayer alertDisplayer)
        {
            _alertDisplayer = alertDisplayer;
        }

        public ICommand VerifyDataCommand => new Command<string>((e) =>
        {
            if (Email == "a@a" && Password == "a")
            {
                _alertDisplayer.DisplayAlert("OK", "Login success");
                //TODO process to main ShowViewModel<LoginViewModel>
            }
            else
            {
                _alertDisplayer.DisplayAlert("Fail", "Login failed");
            }
        });

        public string Email
        {
            get => _email;
            set
            {
                if (value == _email) return;
                _email = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(VerifyDataCommand));
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                if (value == _password) return;
                _password = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(VerifyDataCommand));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class Command<T> : ICommand
    {
        private readonly Action<T> _action;

        public Command(Action<T> action)
        {
            _action = action;
        }

        public bool CanExecute(object parameter)
        {
            if (parameter is T)
            {
                return true;
            }

            return false;
        }

        public void Execute(object parameter)
        {
            _action.Invoke((T)parameter);
        }

        public event EventHandler CanExecuteChanged;
    }

    public class Command : ICommand
    {
        private readonly Action _action;

        public Command(Action action)
        {
            _action = action;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _action.Invoke();
        }

        public event EventHandler CanExecuteChanged;
    }
}