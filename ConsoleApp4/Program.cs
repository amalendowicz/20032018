﻿using System;
using Common;

namespace ConsoleApp4
{
    class Program
    {
        private static MainViewModel _mainViewModel;

        static void Main(string[] args)
        {
            _mainViewModel = new MainViewModel(new ConsoleAlertDisplayer());

            Console.WriteLine("Podaj email");
            var mail = Console.ReadLine();
            Console.WriteLine("Podaj hasło");
            var password = Console.ReadLine();

            _mainViewModel.VerifyData(mail, password);

            Console.ReadLine();
        }
    }

    internal class ConsoleAlertDisplayer : IAlertDisplayer
    {
        public void DisplayAlert(string title, string message)
        {
            Console.WriteLine($"{title} {message}");
        }
    }
}