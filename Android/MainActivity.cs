﻿using System;
using Android.App;
using Android.Content;
using Android.Widget;
using Android.OS;
using Common;
using MvvmCross.Droid.Views;

namespace Android
{
    [Activity(Label = "Android", NoHistory = true, Icon = "@mipmap/icon")]
    public class LoginActivity : MvxActivity<LoginViewModel>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_login);
        }
    }

    [Activity(Label = "Android", NoHistory = true, Icon = "@mipmap/icon")]
    public class MainActivity : MvxActivity<MainViewModel>
    {
        int count = 1;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);

            var button = FindViewById<Button>(Resource.Id.button_login);
            button.Click += delegate { button.Text = string.Format("{0} clicks!", count++); };
        }
    }

    public class DroidAlertDisplayer : IAlertDisplayer
    {
        public void DisplayAlert(string title, string message)
        {
            
        }
    }
}

