﻿using System;
using System.ComponentModel;
using System.Windows;
using Common;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainViewModel(new WpfAlertDisplayer());
        }
    }

    public class WpfAlertDisplayer : IAlertDisplayer
    {
        public void DisplayAlert(string title, string message)
        {
            MessageBox.Show(message, title);
        }
    }
}
