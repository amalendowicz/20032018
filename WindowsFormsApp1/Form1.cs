﻿using System;
using System.Windows.Forms;
using Common;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private MainViewModel _mainViewModel;

        public Form1()
        {
            InitializeComponent();
            _mainViewModel = new MainViewModel(new WinformsAlertDisplayer());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _mainViewModel.VerifyData(textBox1.Text, textBox2.Text);
        }
    }

    public class WinformsAlertDisplayer : IAlertDisplayer
    {
        public void DisplayAlert(string title, string message)
        {
            MessageBox.Show(message, title);
        }
    }
}
